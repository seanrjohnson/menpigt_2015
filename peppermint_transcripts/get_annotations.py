import pandas as pd
from yasmenv import model


tair_protein_process = lambda x: str(x).strip().split(".")[0]
uniprot_protein_process = lambda x: str(x).strip().split("|")[1]

annotation_sources = {"arnold": {"rxns": "../Arabidopsis_Core/arnold_prot_to_rxn.tsv", "identity": "../Arabidopsis_Core/peppermint_peptides_to_arnold.outfmt6.aligned", "compartments":None, "protein_process":tair_protein_process}, "aracyc": {"rxns": "../AraCyc/aracyc_prot_to_rxn.tsv", "identity": "../AraCyc/peppermint_peptides_to_aracyc.outfmt6.aligned", "compartments":"../AraCyc/aracyc_suba.txt", "protein_process":tair_protein_process}, "metacyc": {"rxns": "../MetaCyc/metacyc_prot_to_rxn.tsv", "identity": "../MetaCyc/peppermint_peptides_to_metacyc.outfmt6.aligned", "compartments":"../MetaCyc/uniprot_subcellular_localization_table.tab", "protein_process":uniprot_protein_process, "targetp":"../MetaCyc/metacyc_targetp.tsv"}}

uniprot_cc_dict = {"Peroxisome":"peroxisome", "Mitochondrion":"mitochondrion", "Plastid":"plastid", "Vacuole": "vacuole", "Cytoplasm": "cytosol"}
def uniprot_subcellular_localization(instr):
  out = list()
  for k,v in uniprot_cc_dict.iteritems():
    if k in instr:
      out.append(v)
  return ",".join(out)

all_contigs = pd.read_csv("annotations.csv")
all_contigs.drop(['effective_length','FPKM','IsoPct','expected_count mature','TPM mature', 'FPKM mature', 'IsoPct mature'], axis=1, inplace=True)
all_contigs.rename(columns={"query":"transcript"}, inplace=True)

def compare_string_sets(query, target):
  return " ".join(query.intersection(target))




#PRIAM
priam = pd.read_table("../priam/seqsHits.tab", index_col=False)
priam.drop(["profile_length",	'align_length',	'query_from',	'query_to',	'profile_from',	'profile_to',	'profile_proportion', 'is_best_overlap',	'found_catalytic_pattern'], axis=1, inplace=True)
priam["transcript"] = priam["query_ID"].str.split("\|").str.get(0)
priam["profile_linked_ECs"] = priam["profile_linked_ECs"].str.replace(';', ' ').str.replace("(", "").str.replace(")", "")
priam = priam.sort('positive_hit_probability', ascending=False)
priam = priam.drop_duplicates('transcript')
end_table = pd.merge(all_contigs, priam, how='left', on="transcript")

for (source, params) in annotation_sources.iteritems():
  #print(params)
  rxns = pd.read_table(params['rxns'],index_col=None)
  ident = pd.read_csv(params["identity"],index_col=0)
  
  ident["transcript"] = ident["query"].str.split("|").str.get(0)
  #print(ident)
  ident["protein"] = ident["subject"].map(params["protein_process"])
  #print(source)
  #print(ident.columns)
  ident.drop(["percent_identity","alignment_length","mismatches","gap_opens","query_start","query_end","subject_start","subject_end","e-value","bit_score"], axis=1, inplace=True)
  
  if params["compartments"] is not None:
    
    if ".tab" in params["compartments"]:
      comps = pd.read_table(params["compartments"])
      comps.rename(columns={'Entry':"protein"}, inplace=True)
      comps['Subcellular location [CC]'] = comps['Subcellular location [CC]'].fillna("")
      comps['compartment'] = comps['Subcellular location [CC]'].map(uniprot_subcellular_localization)
      #print(comps)
      comps = comps[["protein","compartment"]]
      
      ident = pd.merge(ident, comps, how='left', on="protein")
    else: #it's a SUBA file
      suba = pd.read_table(params["compartments"])
      suba['protein'] = suba['AGI identifier'].map(params["protein_process"])
      suba = suba[['protein', 'location_subacon']]
      suba.rename(columns={'location_subacon':'compartment'}, inplace=True)
      ident = pd.merge(ident, suba, how='left', on='protein')
    if "targetp" in params:
      #print(params["compartments"])
      targetp_dict = {"_":"cytosol", "C":"plastid", "M":"mitochondrion", "S":"cytosol", "*":"cytosol"}
      targetp = pd.read_table(params["targetp"])
      targetp.drop(['Len','cTP','mTP','SP','other', 'RC', 'TPlen'], axis=1, inplace=True)
      targetp['targetp'] = targetp['Loc'].map(targetp_dict)
      targetp['protein'] = targetp["Name"].map(params["protein_process"])
      targetp.drop(['Loc'], axis=1, inplace=True)
      
      ident = pd.merge(ident, targetp, how='left', on="protein")
      ident.drop(["Name"], axis=1, inplace=True)
  
  ident.drop("subject", axis=1, inplace=True)
  ident.sort('global_ident', ascending=False, inplace=True)
  ident.drop_duplicates('transcript', inplace=True)
  
  rxns.rename(columns={'gene':'protein'}, inplace=True)
  ident = pd.merge(ident, rxns, how='left', on='protein')
  
  new_col_names = {x: source + "_" + x for x in ident.columns if x != 'transcript'}
  ident.rename(columns=new_col_names, inplace=True)
  end_table = pd.merge(left=end_table, right=ident, how='left', on='transcript')
  
end_table.to_csv("transcripts_with_protein_annotations.csv")