import pandas as pd

annotations_table_file = "transcripts_with_protein_annotations.csv"

annots = pd.read_csv(annotations_table_file, index_col=0)

ident_cutoff = 70.0
ident_step = 5.0

def get_reactions_source(s):
  '''
    given a row of the annotations table, determine which (if any) source to assign reactions from
  '''
  out = ""
  if s['max_ident'] >= ident_cutoff:
    max_col = s[["metacyc_global_ident","aracyc_global_ident","arnold_global_ident"]].idxmax()
    if "arnold" in max_col:
      out = "arnold"
    elif "aracyc" in max_col:
      if s["arnold_global_ident"] + 5.0 >= s['max_ident']:
        out = "arnold"
      else:
        out = "aracyc"
    elif "metacyc" in max_col:
      if s["arnold_global_ident"] + 5.0 >= s['max_ident']:
        out = "arnold"
      elif s["aracyc_global_ident"] + 5.0 >= s['max_ident']:
        out = "aracyc"
      else:
        out = "metacyc"
    else:
      print("Error, could not determine best identity for transcript " + s['transcript'])
  #print(out)
  return out


def get_compartments(s):
  '''
    given a row of the annotations file (including a column called "rxn_source" telling where to get the reactions from) generate a list of reactions with compartments indicated by: _[letter]
  '''
  compartments = set()
  #reactions = list()
  if s['rxn_source'] == 'arnold':
    reactions = s['arnold_Reactions'].split()
  elif s['rxn_source'] == 'aracyc':
    compartments.update(set(s['aracyc_compartment'].split(",")))
    #reactions = s['aracyc_Reactions'].split()
    if len(compartments) == 0:
      compartments.add("cytosol")
  elif s['rxn_source'] == 'metacyc':
    if s['metacyc_compartment'] != "":
      compartments.update(set(s['metacyc_compartment'].split(",")))
    else:
      compartments.update(set(s['metacyc_targetp'].split(",")))
    
    #reactions = s['metacyc_Reactions'].split()
    if len(compartments) == 0:
      compartments.add("cytosol")
  else:
    print("Error, don't understand reaction source for " + s['transcript'])
  
  # out = list()
  # if len(compartments) == 0:
    # out = reactions
  # for compartment in compartments:
    # c = compart_map.get(compartment)
    # for reaction in reactions:
      # out.append(reaction + "_" + c)
  return ",".join(compartments)

annots.ix[:,["metacyc_global_ident","aracyc_global_ident","arnold_global_ident"]] = annots.ix[:,["metacyc_global_ident","aracyc_global_ident","arnold_global_ident"]].fillna(0.0)
annots.ix[:,["metacyc_compartment","aracyc_compartment"]] = annots.ix[:,["metacyc_compartment","aracyc_compartment"]].fillna("")
annots['max_ident'] = annots[["metacyc_global_ident","aracyc_global_ident","arnold_global_ident"]].max(axis=1)
annots = annots[annots['max_ident'] > 0.01]
annots['rxn_source'] = annots.apply(get_reactions_source, axis=1)
annots = annots[annots['rxn_source'] != ""]
annots['source_ident'] = annots.apply(lambda x: x[x['rxn_source']+"_global_ident"], axis=1)
annots['reactions'] = annots.apply(lambda x: x[x['rxn_source']+"_Reactions"], axis=1)
annots['compartments'] = annots.apply(get_compartments, axis=1)


annots = annots[['transcript', 'rxn_source', 'source_ident', 'reactions', "compartments"]]
annots.to_csv("transcript_reaction_associations.tsv", sep="\t")