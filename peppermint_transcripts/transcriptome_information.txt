Number of contigs in assembly:
16278

Number of transdecoder peptides:
10109

Number of contigs with peptides:
9878

Number of contigs with uniprot (AraCyc + MetaCyc + Arnold) hits:
9748

Number of contigs with PRIAM hits:
6467