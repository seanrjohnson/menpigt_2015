import pandas as pd
from yasmenv import model

t = pd.read_table("uniprot_table.tab")

model_names = ("instantiated_good", "instantiated_bad")

for model_name in model_names:

  proteins_column = "Proteins"

  m = model.from_tsv(model_name + ".tsv")


  gene_map = dict()
  for (i, r) in t.iterrows():
    ara_genes = r['Query'].strip().split(",")
    uniprot = r['Entry']
    for g in ara_genes:
      if g not in gene_map:
        gene_map[g] = set()
      gene_map[g].add(uniprot)

  for k,v in gene_map.iteritems():
    if len(v) == 0:
      print("Error, no uniprot IDs for gene " + k)
    elif len(v) == 1:
      gene_map[k] = list(v)[0]
    else:
      gene_map[k] = " ".join(v)
      
  for r in m:
    gra = m[r].annotations[proteins_column]
    
    new_gra = gra
    for k, v in gene_map.iteritems():
      new_gra = new_gra.replace(k,v)
    
    m[r].annotations[proteins_column] = new_gra

  m.to_tsv(model_name + "_updated_uniprot.tsv")