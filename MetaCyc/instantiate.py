#
#   generates instantiated reactions from a biocyc database
#
#   sorts instantiated reactions into two categories
#   "good": reactions and instances that are balanced and unambiguous
#   "bad": reactions that have no balanced instantiations
#           ambiguous instantiations
#           reactions that are not generic, but are not balanced
#

import yasmenv.biocyc as bc
from pprint import pprint as pp

goodoutfilename = "instantiated_good.tsv" #change
badoutfilename = "instantiated_bad.tsv" #change

out_header = "Rxn name\tInstance\tDescription\tFormula\tProteins\tSubsystems\tLB\tUB\tObjective\tEC\tparents\tancestors\tSpontaneous\tNotes"

def instantiated_reaction_string(name, instance, instance_num, reactions, enzrxns, proteins):
    bool_dict = {True:'1', False:'0'}
    description = reactions[name].description()
    formula = bc.reaction_as_str(instance['reaction'])
    proteins = " ".join(reactions[name].uniprot(enzrxns, proteins))
    subsystems = " ".join(reactions[name].subsystems())
    (LB, UB) = reactions[name].bounds()
    objective = '0'
    ecs = " ".join(reactions[name].ecs())
    parent_reactions = " ".join(reactions[name].parent_reactions(reactions))
    ancestor_reactions = " ".join(reactions[name].ancestor_reactions(reactions))
    spontaneous = bool_dict[reactions[name].spontaneous()]
    notes = instance['tag']

    
    return "\t".join([name, str(instance_num), description, formula, proteins, subsystems, LB, UB, objective, ecs, parent_reactions, ancestor_reactions, spontaneous, notes])

def instantiate():
    classes = bc.Dat("classes.dat", rec_class=bc.Class) #change
    compounds = bc.Dat("compounds.dat", rec_class=bc.Compound) #change
    reactions = bc.Dat("reactions.dat", rec_class=bc.Reaction) #change
    proteins = bc.Dat("proteins.dat", rec_class=bc.Protein) #change
    enzrxns = bc.Dat("enzrxns.dat", rec_class=bc.Enzrxn) #change
    
    
    
    with open(goodoutfilename, 'w') as goodout:
        goodout.write(out_header)
        goodout.write("\n")
        with open(badoutfilename, 'w') as badout:
            badout.write(out_header)
            badout.write("\n")
            for rxn_name in reactions.all_records().keys():
                #(balanced_include, unbalanced, balanced_not_include)
                (balanced_include, unbalanced, balanced_not_include) = reactions[rxn_name].instances(compounds, classes)
                for (num, rxn) in enumerate(balanced_include):
                    goodout.write(instantiated_reaction_string(rxn_name, rxn, num, reactions, enzrxns, proteins))
                    goodout.write("\n")
                for (num, rxn) in enumerate(unbalanced):
                    badout.write(instantiated_reaction_string(rxn_name, rxn, num, reactions, enzrxns, proteins))
                    badout.write("\n")
                for (num, rxn) in enumerate(balanced_not_include):
                    badout.write(instantiated_reaction_string(rxn_name, rxn, num, reactions, enzrxns, proteins))
                    badout.write("\n")


if __name__ == "__main__":
    instantiate()