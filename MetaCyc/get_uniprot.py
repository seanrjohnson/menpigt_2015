from yasmenv import model

source_files = ('instantiated_bad.tsv', 'instantiated_good.tsv')

gene_set = set()

for file in source_files:
  m = model.from_tsv(file)
  for r in m:
    gene_set.update(set(m[r].annotations['Proteins'].strip().split()))

print("\n".join(gene_set))