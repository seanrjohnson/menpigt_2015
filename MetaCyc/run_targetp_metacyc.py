import pandas as pd
import subprocess
import re
from glob import glob
#TODO: there is a bug in this code, where the rows do not all have the same number of columns
targetp_path = "/home/sean/Desktop/assembly_software/targetp-1.1/targetp"
query_dir= "uniprot_proteins"
tab = pd.read_table("uniprot_taxonomy_table.tab")
#tax = pd.read_table("metacyc_19_1/taxonomy-all.tab", usecols=("Scientific name", "Lineage"))
#tax.dropna(subset=["Lineage"], inplace=True)

#tax["plant"] = tax["Lineage"].map(lambda x: True if "Viridiplantae;" in x else False)

#tab = pd.merge(tab, tax, how="left", left_on="Organism", right_on="Scientific name")
#outfile = "individual_uniprot/"
tab['Taxonomic lineage (KINGDOM)'] = tab['Taxonomic lineage (KINGDOM)'].fillna("")
output_header = "Name\tLen\tcTP\tmTP\tSP\tother\tLoc\tRC\tTPlen"


print(output_header)
for (i,r) in tab.iterrows():
	p = "-N"
	if r['Taxonomic lineage (KINGDOM)'] == 'Viridiplantae':
		p = "-P"
	fname = r['Entry']
	g = glob(query_dir+"/*"+fname+"*.fasta")
	if len(g) > 1:
		
		print("ERROR!, wrong number of files!")
	for f in glob(query_dir+"/*" + fname + "*.fasta"):
		command = targetp_path
		
		proc = subprocess.Popen(["sh", command, "-c", p, f], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		(out, err) = proc.communicate()
		#print(out)
		#print(err)
		lines = out.split("\n")
		for line in lines:
			match = re.match(".*\|",line)
			if match:
				print("\t".join(line.split()))
