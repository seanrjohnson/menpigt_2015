import pandas as pd
import subprocess
import re

filename = "peppermint_peptides_to_uniprot.outfmt6"
query_dir= "../peppermint_transcripts/individual_peptides"
subject_dir = "uniprot_proteins"
align0_path = "/home/sean/Desktop/assembly_software/fasta-21/align0"


df = pd.read_table(filename)

for i in range(len(df)):
  query = df.ix[i,'query']
  subject = df.ix[i,'subject']
  
  query = query_dir + "/" + query + ".fasta"
  subject = subject_dir + "/" +  subject + ".fasta"
  
  align0_p = subprocess.Popen([align0_path, "-m 10", query, subject], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  #(out, err) = align0_p.communicate()
 
  to_fasta_p = subprocess.Popen(["python", "../align0_m10_to_fasta.py"], stdin = align0_p.stdout, stdout=subprocess.PIPE)
  #q = to_fasta_p.communicate()[0]
  
  identity_p = subprocess.Popen(["python", "../calculate_identity.py", "--identity_type", "align"], stdin = to_fasta_p.stdout, stdout=subprocess.PIPE)
  
  ident = identity_p.communicate()[0]
  print(ident)
  df.loc[i, "global_ident"] = ident
df.to_csv(filename + ".aligned")
