#! /usr/bin/python
'''
  Reads a file produced by align0 from Fasta2 using the -m 10 option, writes a fasta file
  
  Usage:
    align0_m10_to_fasta.py -i input.m10 -o output.fasta
    cat input.m10 | align0_m10_to_fasta.py
'''
import sys
import argparse

def split_fasta(input_handle, output_handle):
  state = "header"
  for line in input_handle:
    line = line.strip()
    if len(line)>0:
      if line[0] == ";":
        pass # do nothing
      elif line[0] == ">":
        state = "body"
        output_handle.write(line + "\n")
      elif state == "body":
        output_handle.write(line + "\n")
    

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None)
  parser.add_argument("-o", default=None)
  
  args = parser.parse_args()
  
  if args.i is not None:
    input_handle=open(args.i, "rU")
  else:
    input_handle = sys.stdin
  
  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout
  
  split_fasta(input_handle, output_handle)
  
  input_handle.close()
  output_handle.close()
