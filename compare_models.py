import yasmenv.model as model
import yasmenv.gurobi_solver as solver_lib
import pandas as pd
import yasmenv.modelreaction as modelreaction
solver = solver_lib.Solver()

models = {"1":"first_model.tsv", "2":"second_model.tsv", "3":"third_model.tsv", "4":"fourth_model.tsv", "5":"fifth_model.tsv"}

output_filename = "model_comparison.tsv"

inf = float('inf')

def main():
  stats = dict()
  for (mname, mfile) in models.iteritems():
    stats[mname] = dict()
    m = model.from_tsv(mfile)
    
    stats[mname]['reactions'] = m.len_reactions()
    stats[mname]['metabolites'] = m.len_metabolites()
    
    singlet_metabolites = m.find_singlets()
    no_singlets = m.remove_reactions_with_metabolites(singlet_metabolites)
    
    stats[mname]['singlet metabolites'] = len(singlet_metabolites)
    stats[mname]['reactions with singlets'] = m.len_reactions() - no_singlets.len_reactions()
    
    stats[mname]['peppermint transcripts'] = count_transcripts(m)
    
    stats[mname]['blocked reactions'] = count_blocked_reactions(m)
  df = pd.DataFrame.from_dict(stats, orient='index')
  df.to_csv(output_filename, sep="\t")
  
  
def count_transcripts(m):
  transcripts = set()
  for r in m.reactions_list():
    transcripts = transcripts.union(set(r.annotations['transcript'].strip().split(" ")))
    
  return len(transcripts)

def str_to_float(input):
  out = 0.0
  if input == "(+)inf":
    out = inf
  elif input == "(-)inf":
    out = -1*inf
  else:
    out = float(input)
  return out
  
def count_blocked_reactions(m):
  m2 = m.make_finite()
  for r_name in m2:
    m2[r_name].objective = 0
  if 'Ex_Raffinose' not in m2:
    r = modelreaction.ModelReaction("Ex_Raffinose",  left_side={"CPD-1099[c]":1}, right_side={}, lb=-10, ub=0, objective=0)
    m2.add_reaction(r)
  m2['Ex_Raffinose'].lb = -1000
  m2['Ex_Raffinose'].ub = 0
  
  if 'Im_hnu' in m2:
    m2['Im_hnu'].lb = 0
    m2['Im_hnu'].ub = 0
  
  m2['Im_CO2'].lb = -1000
  m2['Im_CO2'].ub = 1000
  
  m2['Im_H2O'].lb = -1000
  m2['Im_H2O'].ub = 1000
  
  m2['Ex_O2'].lb = -1000
  m2['Ex_O2'].ub = 1000
  
  solution = solver.fva(m)
  blocked = 0
  for sol in solution['fluxes']:
    
    maximum = max(abs(str_to_float(sol[1])), abs(str_to_float(sol[2])))
    if maximum < 0.001:
      blocked += 1
  return blocked
    
  
if __name__ == "__main__":
  main()