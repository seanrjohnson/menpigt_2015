import yasmenv.model as model
gra_parse = model.GraParser()
m = model.from_tsv('Arnold_metacyc.tsv')
genes = set()
for r in m:
  genes |= gra_parse.get_genes_from_bool(m[r].annotations['Gene-reaction association'])
print("\n".join(genes))