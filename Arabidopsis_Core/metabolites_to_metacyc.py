import pandas as pd
from yasmenv import model

mets = pd.read_table('metabolites.tsv')
m = model.from_tsv("Arnold_2014.tsv")

met_dict = dict()

for (i, r) in mets.iterrows():
  met = r['Metabolite name'][:-3]
  metacyc = r['MetaCyc']
  if met in met_dict:
    if metacyc != met_dict[met]:
      print("inconsistent naming: %s, %s, %s" % (met, met_dict[met], metacyc))
  else:
    met_dict[met] = metacyc

merge_dict = dict()
for (arnold, metacyc) in met_dict.iteritems():
  if metacyc in merge_dict:
    if arnold not in merge_dict[metacyc]:
      merge_dict[metacyc].append(arnold)
  else:
    merge_dict[metacyc] = [arnold]

m2 = m.merge_metabolites(merge_dict, True)

m2.to_tsv('Arnold_metacyc.tsv')