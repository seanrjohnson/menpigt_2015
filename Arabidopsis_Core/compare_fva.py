import yasmenv.gurobi_solver as solver_lib
import yasmenv.model as model
import yasmenv.util as util
import yasmenv.gurobi_solver as solver_lib

m = model.from_tsv("Arnold_2014.tsv")
m2 = model.from_tsv("Arnold_metacyc.tsv")

solver = solver_lib.Solver()

util.to_file("original_fva.tsv", util.format_fva(solver.fva(m)))
util.to_file("metacyc_fva.tsv", util.format_fva(solver.fva(m2)))