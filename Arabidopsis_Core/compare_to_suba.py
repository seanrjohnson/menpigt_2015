import pandas as pd
from yasmenv import model
gra_parse = model.GraParser()

#subacon_compartments_dict = {'cytosol':"cytosol", 'endoplasmic reticulum':"cytosol", 'extracellular':'extracellular', 'golgi':"cytosol", 'mitochondrion':"mitochondrion", 'nucleus': 'nucleus', 'peroxisome':'peroxisome', 'plasma membrane':'cytosol',  'plastid':'plastid', 'vacuole':'vacuole'}
subacon_compartments_dict = {'cytosol':"cytosol", 'endoplasmic reticulum':"cytosol", 'extracellular':'extracellular', 'golgi':"cytosol", 'mitochondrion':"mitochondrion", 'nucleus': 'cytosol', 'peroxisome':'peroxisome', 'plasma membrane':'cytosol',  'plastid':'plastid', 'vacuole':'cytosol'}
arnold_compartments_dict = {"c": "cytosol", 'h': 'plastid', 'i': 'mitochondrion', 'l': 'plastid', 'm': 'mitochondrion', 'p': 'peroxisome'}

arnold_compartments = dict() #keys are gene_names, values are sets of compartments
subacon_compartments = dict()

arnold = model.from_tsv('Arnold_metacyc.tsv')
subacon = pd.read_table("arnold_suba.tsv")

def add_suba_compartment(series, compart_dict):
  gene = series['AGI identifier'].strip().split(".")[0]
  locations = set(series['location_subacon'].strip().split(","))
  compartments = {subacon_compartments_dict[x] for x in locations}
  
  if gene not in compart_dict:
    compart_dict[gene] = set()

  compart_dict[gene].update(compartments)

subacon.apply(add_suba_compartment, axis=1, args=[subacon_compartments])

for r in arnold:
  genes = gra_parse.get_genes_from_bool(arnold[r].annotations['Gene-reaction association'])
  compartments = set()
  sides = ("left", "right")
  for side in sides:
    for met in getattr(arnold[r], side):
      c = model.get_compartment(met)
      compartments.add(arnold_compartments_dict[c])
  for gene in genes:
    if gene not in arnold_compartments:
      arnold_compartments[gene] = set()
    arnold_compartments[gene].update(compartments)

num_different = 0
num_same = 0
num_partial_intersetion = 0
for gene in subacon_compartments:
  subacon_comps = subacon_compartments[gene]
  arnold_comps = arnold_compartments[gene]
  same = True
  combined = subacon_comps.union(arnold_comps)
  if len(combined.difference(subacon_comps)) > 0 or  len(combined.difference(arnold_comps)) > 0:
    same = False
    num_different += 1
    if len(subacon_comps.intersection(arnold_comps)) > 0:
      num_partial_intersetion += 1
  else:
    num_same += 1
  print(gene + "\t" + " ".join(subacon_comps) + "\t" + " ".join(arnold_comps) + "\t" + str(same))
print("different:\t" + str(num_different))
print("same:\t" + str(num_same))
print("partial same:\t" + str(num_partial_intersetion))