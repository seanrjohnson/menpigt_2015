#! /usr/bin/python
'''
  Reads a Fasta file, writes each sequence in the fasta file to its own file
  
  
  
  Usage:
    split_fasta.py -i input.fasta --output_dir output_directory
    cat input.fasta | split_fasta.py --output_dir output_directory
'''
import sys
import argparse
from Bio import SeqIO
import os

def split_fasta(input_fasta_handle, output_dir):
    
  for seq in SeqIO.parse(input_fasta_handle, 'fasta'):
    
    with open(os.path.join(output_dir, seq.name + ".fasta"), "w") as output_handle:
      SeqIO.write(seq, output_handle, 'fasta')


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None)
  parser.add_argument("--output_dir", default=".")
  
  args = parser.parse_args()
  
  if args.i is not None:
    input_handle=open(args.i, "rU")
  else:
    input_handle = sys.stdin
  
  split_fasta(input_handle, args.output_dir)


  #if args.o is not None:
   # output_handle = open(args.o, "w")
  #else:
  #  output_handle = sys.stdout
