#! /usr/bin/python
'''
  Reads an alignment file, calculates sequence identity
  
  Usage:
    split_fasta.py -i input.m10 -o output.fasta
    cat input.m10 | split_fasta.py
    
  TODO: alignment that ignores end gaps
'''
import sys
import argparse
from Bio import SeqIO
from Bio import AlignIO

def read_alignment(input_handle, file_type):
  return AlignIO.read(input_handle, file_type)


def calculate_identity(alignment, ident_type):
  identities = 0
  total_length = 0
  lengths = {"global":0, "big":0, "small":0, "align":0}
  
  
  if len(alignment) > 0:
    lengths['global'] = len(alignment[0])
    for i in range(len(alignment[0])):
      if identical(alignment[:,i]):
        identities += 1
      if not gap(alignment[:,i]):
        lengths['align'] += 1
  seq_lens = []
  
  for seq in alignment:
    seq_lens.append(len(seq.seq.tostring().replace("-","")))
  
  if len(seq_lens) > 0:
    lengths['big'] = max(seq_lens)
    lengths['small'] = min(seq_lens)
  
  total_length = lengths[ident_type]
  
  out = 100.0
  if total_length > 0:
    out = 100.0 * float(identities)/total_length
    
  return out



def identical(string):
  '''
  returns true if all the characters in the string are the same, else false
  '''
  out = True
  if len(string) > 0:
    st = string[0]
    for x in string:
      if st != x:
        out = False
  return out
  
def gap(string):
  '''
  returns true if there is at least one "-" in the string, else false
  '''
  out = True
  if string.find("-") == -1:
    out = False
  return out
  

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default=None)
  parser.add_argument("-o", default=None)
  parser.add_argument("-t", default="fasta",help="file type")
  parser.add_argument("--identity_type", default="global", choices=["global","big","small","align"], help="type of identity to calculate: global, big, small, align")
  #identity types as defined in Tian and Skolnick, 2003, J Mol Biol 333:863-882
  
  args = parser.parse_args()
  
  if args.i is not None:
    input_handle=open(args.i, "rU")
  else:
    input_handle = sys.stdin
  
  if args.o is not None:
    output_handle = open(args.o, "w")
  else:
    output_handle = sys.stdout
  
  ident = calculate_identity(read_alignment(input_handle, args.t), args.identity_type)
  
  output_handle.write("%.2f" % ident)
  
  input_handle.close()
  output_handle.close()
