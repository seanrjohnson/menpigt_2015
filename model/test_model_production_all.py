import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib
from yasmenv import util

inf = float('inf')
solver = solver_lib.Solver()

m = model.from_tsv('fifth_model.tsv')
output_suffix = "test_model_production"

m = m.make_finite(smallest_lb=-10000, largest_ub=10000)
for met in m.metabolite_names():
  #ModelReaction(self, name, left_side, right_side, lb, ub, objective, expression=None, annotations = dict())
  r = modelreaction.ModelReaction(met + "_test_export",  left_side={met:1}, right_side={}, lb=0, ub=10000, objective=0)
  m.add_reaction(r)

#zero out the objective function
for r_name in m:
  m[r_name].objective = 0

m.to_tsv("model_" + output_suffix + ".tsv")
  
solution = solver.fva(m)
out_string = ""
for sol in solution['fluxes']:
  sol = [str(x) for x in sol]
  out_string += "\t".join(sol) + "\n"

util.to_file("fva_sol_" + output_suffix + ".tsv", out_string)
