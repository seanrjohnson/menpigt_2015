import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib

inf = float('inf')

solver = solver_lib.Solver()
#m = model.from_multiple('model_production_files.txt')
m = model.from_tsv('fifth_model.tsv')

compound_list_filename = 'biomass_compounds.txt'

#zero out the objective function
for r_name in m:
    m[r_name].objective = 0

#uncomment to add ponciretin biosynthesis
# ModelReaction(self, name, left_side, right_side, lb, ub, objective, expression=None, annotations = dict())
#r = modelreaction.ModelReaction("ponciretin-biosynthesis",  left_side={"NARINGENIN-CMPD":1, "S-ADENOSYLMETHIONINE":1}, right_side={"ADENOSYL-HOMO-CYS":1, "CPD-7080":1, "PROTON":2}, lb=0, ub=1000, objective=0)
#m.add_reaction(r)

m['Ex_Glc'].lb = 0
m['Ex_Suc'].lb = 0
m['Ex_Suc'].ub = 0
m['Ex_Raffinose'].lb = -10
m['Ex_Raffinose'].ub = 0

#test every metabolite for ability to be produced
for met in model.list_from_file_from_multiple(compound_list_filename, '#'):
#for met in m.metabolite_names():
    #(self, name, left_side, right_side, lb, ub, objective, expression=None)
    new_rxn_name = met + "production_capability"
    while new_rxn_name in m:
      new_rxn_name += "_"
    new_rxn = modelreaction.ModelReaction(new_rxn_name, {met:1}, {}, 0, 1000, 1)
    #new_model = m.copy()
    m.add_reaction(new_rxn)
    solution = solver.fba(m, maximize=True)
    if solution is None or solution['obj'] == 0:
        print "cannot produce " + met
    else:
        print "can produce " + met + " obj " + str(solution['obj'])
    m[new_rxn_name].objective = 0
    m[new_rxn_name].ub = 0
