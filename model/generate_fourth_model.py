from yasmenv import model
import yasmenv.gurobi_solver as solver_lib
solver = solver_lib.Solver()

m = model.from_tsv("third_model.tsv")

m['Ex_Suc'].lb = 0
m['Ex_Suc'].ub = 0
m['Ex_Raffinose'].lb = -10
m['Ex_Raffinose'].ub = 0

m['Ex_O2'].lb = -1000
m['Ex_O2'].ub = 0

m['Biomass'].objective = 1
m['Biomass'].lb = 0
m['Biomass'].ub = 1000

#m['Ex_menthol'].objective = 1
#m['Ex_menthol'].lb = 0
#m['Ex_menthol'].ub = 1000

m2 = model.Model()

sol = solver.fba(m, maximize=True)
obj_original = sol['obj']
#print sol['obj']
lower = list()
for r in m.reactions_list():
  
  keep = True
  if r.annotations['source_model'].strip() == "arnold":
    if r.annotations['Genes'].strip() != "-":
      if r.annotations['transcript'].strip() == "":
        lb = r.lb
        ub = r.ub
        r.lb = 0
        r.ub = 0
        solution = solver.fba(m, maximize=True)
        r.lb = lb
        r.ub = ub
        if solution is None or solution['obj'] == 0: #the reaction is necessary for biomass production
          pass
        elif solution['obj']+ 0.001 < obj_original :
          lower.append((str(r.name), str(solution['obj'])))
          keep = False
        else:
          keep = False
        
  if keep == True:
    m2.add_reaction(r)

m['Biomass'].lb = 0
m['Biomass'].ub = 1000

m2.to_tsv("fourth_model.tsv")

sol = solver.fba(m2, maximize=True)

print(obj_original)
print ("\n".join([x +"\t" +y for x,y in lower]))
print(sol['obj'])