import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib
#import yasmenv.linearprogramming as solver_lib
from yasmenv import util

inf = float('inf')

solver = solver_lib.Solver()
m = model.from_tsv('fifth_model.tsv')
#m = model.from_multiple('model_production_files.txt')
#m = model.from_tsv('fourth_model.tsv')

out_suffix = "single_knockout"

#zero out the objective function
for r_name in m:
    m[r_name].objective = 0

#limit glucose uptake
#m['Ex_Glc'].lb = -10
m['Ex_Suc'].lb = 0
m['Ex_Suc'].ub = 0
#m['Im_NH4'].lb = 0
#m['Im_NH4'].ub = 0
m['Ex_Raffinose'].lb = -1000
m['Ex_Raffinose'].ub = 0
m['Ex_Raffinose'].objective = 1


m['Biomass'].objective = 0
m['Ex_menthol'].objective = 0
m['Ex_menthol'].lb = 1
m['Ex_menthol'].ub = 1


#solve the fba problem
fbasol = solver.single_knockout(m, maximize=True)
#print(fbasol['obj'])

util.to_file(out_suffix + "_fba_sol.tsv", util.format_list_of_tuples(fbasol['kos']))

# m2 = model.Model()
# for (rxn, flux) in fbasol['kos']:
  # if abs(flux) > 0.01:
    # m2.add_reaction(m[rxn])

# #ignore = ['ATP', 'NADP', 'NADPH', 'NAD', 'NADH', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
# ignore = ['NADP', 'NAD', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
# m2.to_sif('fba_graph_fourth_2_3.sif', ignore, fbasol['fluxes'], ignore_compartmentalization=True)
# m2.to_tsv('fba_model_fourth_2_3.tsv')
