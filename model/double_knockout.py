import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib
#import yasmenv.linearprogramming as solver_lib
from yasmenv import util

inf = float('inf')

solver = solver_lib.Solver()
m = model.from_tsv('fifth_model.tsv')
#m = model.from_multiple('model_production_files.txt')
#m = model.from_tsv('fourth_model.tsv')
#m = m.remove_metabolites(["Pi","WATER"],all_compartments=True) 
out_suffix = "double_knockout_no_NH4"

#zero out the objective function
for r_name in m:
    m[r_name].objective = 0

#r = modelreaction.ModelReaction("export",  left_side={"PHE[c]":1}, right_side={}, lb=0, ub=1000, objective=1)
#m.add_reaction(r)
#m['export'].objective = 1
    
#limit glucose uptake
#m['glucose_exchange'].lb = -1
#m['glucose_exchange'].ub = 0

#limit glucose uptake
#m['Ex_Glc'].lb = -10
m['Ex_Suc'].lb = 0
m['Ex_Suc'].ub = 0
m['Ex_Raffinose'].lb = -1000
m['Ex_Raffinose'].ub = 0
m['Ex_Raffinose'].objective = 1


m['Biomass'].objective = 0
m['Ex_menthol'].objective = 0
m['Ex_menthol'].lb = 1
m['Ex_menthol'].ub = 1

# m['GABATA2_m'].lb = 0
# m['GABATA2_m'].ub = 0

# m['GluDH3NADP_c'].lb = 0
# m['GluDH3NADP_c'].ub = 0

# m['GABATA1_m'].lb = 0
# m['GABATA1_m'].ub = 0

# m['AlaTA_m'].lb = 0
# m['AlaTA_m'].ub = 0

# #RXN0-5224|0 turn off CO2 => HCO3
# m['RXN0-5224|0'].lb = 0.0
# m['RXN0-5224|0'].ub = 0.0

#adjust the ATP demand
#m["3.6.3.1-RXN|0_aracyc_good_c"].lb = 300
#m["3.6.3.1-RXN|0_aracyc_good_c"].ub = 300

#m["3.6.3.1-RXN|0_aracyc_good_m"].lb = 300
#m["3.6.3.1-RXN|0_aracyc_good_m"].ub = 300

#solve the fba problem
fbasol = solver.double_knockout(m, maximize=True)
#print(util.format_list_of_tuples((x for x in fbasol['kos'] if x[2] < 0.999)))
#util.to_file(out_suffix + "_fba_sol.tsv", "'" + str(fbasol['obj']) + "\n")
util.to_file(out_suffix + "_fba_sol.tsv", util.format_list_of_tuples(fbasol['kos']))


# m2 = model.Model()
# for (rxn, flux) in fbasol['kos']:
  # if abs(flux) > 0.01:
    # m2.add_reaction(m[rxn])

# #ignore = ['ATP', 'NADP', 'NADPH', 'NAD', 'NADH', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
# ignore = ['NADP', 'NAD', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
# m2.to_sif('fba_graph_fourth_2_3.sif', ignore, fbasol['fluxes'], ignore_compartmentalization=True)
# m2.to_tsv('fba_model_fourth_2_3.tsv')
