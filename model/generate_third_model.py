from yasmenv import model
import pandas as pd

#previous = model.from_multiple('model_production_files.txt')
#model.from_tsv("third_model_2.tsv").make_finite()
previous = model.from_tsv("second_model.tsv").make_finite()
old_fourth = model.from_tsv("original_fourth_model.tsv").make_finite() #what I eventually called the "third model" was originally called the "fourth model"
delete_list = set(pd.read_table("reactions_to_delete_for_third_model.txt", header=None)[0].tolist())
#print(delete_list)
comp_set = {"forward", "reverse"}

not_found = list()
for new_r in previous.reactions_list():
  found = False
  for old_r in old_fourth.reactions_list():
    comp = old_r.compare(new_r, net=True)
    if len(comp_set.intersection(comp)):
      found=True
      lb = old_r.lb
      ub = old_r.ub
      if 'reverse' in comp:
        lb = -1 * ub
        ub = -1 * lb
      if lb != new_r.lb or ub != new_r.ub:
        if (lb == 0 or ub == 0) and ('manual' not in new_r.annotations['source_model']):
          print("\t".join([old_r.name, new_r.name, str(lb), str(ub), str(new_r.lb), str(new_r.ub)]))
          new_r.lb = lb
          new_r.ub = ub
  if not found:
    not_found.append(new_r.name)
        
print("\n".join(not_found))

new_model = model.Model()

{'RIBOSE-5P': ['CPD-15895','CPD-15318','CPD-16551']}

for r in previous.reactions_list():
  if r.name not in delete_list:
    if len(set(r.annotations['merged_reactions'].split()).intersection(delete_list)) < 1:
      new_model.add_reaction(r)

manual = model.from_tsv("manual_reactions_to_generate_model_three.tsv")
manual.merge_in(new_model.copy())
manual = manual.merge_metabolites({'RIBOSE-5P': ['CPD-15895','CPD-15318','CPD-16551']}, True)

manual.to_tsv("third_model.tsv")
#compare
