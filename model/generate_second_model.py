from yasmenv import model

m = model.from_tsv("first_model.tsv")

(m_clean, clean_map) = m.clean(merge_args={'metabolites_merge':max, 'annotations_sep':{"Description":",", "Subsystems":",", "Gene-reaction association":','}})
#reverse_clean_map = dict()
#print(clean_map)

for r in m_clean:
  old_rxns = set()
  for (r_old, r_new) in clean_map.iteritems():
    if r_new is not None and r_new['name'] == r:
      old_rxns.add(r_old)
  m_clean.add_annotation(r, "merged_reactions", " ".join(old_rxns))

m_no_bad_instantiations = model.Model()

for r in m_clean:
  #if "instantiation" == m_clean[r].annotations['Notes'].strip() and (m_clean[r].annotations['source_model'].strip() == "metacyc_bad" or m_clean[r].annotations['source_model'].strip() == "aracyc_bad" or len(set(["aracyc_bad", "metacyc_bad"]).intersection(set(m_clean[r].annotations['source_model'].strip().split()))) == 2):
  if (m_clean[r].annotations['source_model'].strip() == "metacyc_bad" or m_clean[r].annotations['source_model'].strip() == "aracyc_bad" or len(set(["aracyc_bad", "metacyc_bad"]).intersection(set(m_clean[r].annotations['source_model'].strip().split()))) == 2):
    pass
  elif "not_balanced" in m_clean[r].annotations['Notes'].strip():
    pass
  else:
    m_no_bad_instantiations.add_reaction(m_clean[r])

manual = model.from_tsv("manual_reactions_to_generate_model_two.tsv")

#model_with_manual = 
manual.merge_in(m_no_bad_instantiations.copy())

(m_clean, clean_map) = manual.clean(merge_args={'metabolites_merge':max, 'annotations_sep':{"Description":",", "Subsystems":",", "Gene-reaction association":','}})

for r in m_clean:
  old_rxns = set()
  for (r_old, r_new) in clean_map.iteritems():
    if r_new is not None and r_new['name'] == r:
      old_rxns.add(r_old)
  m_clean.add_annotation(r, "merged_reactions", " ".join(old_rxns))

# singlets = set(model_with_manual.find_singlets())

# m_no_orphans = model.Model()

# for r in m_no_bad_instantiations:
  # mets = set(m_no_bad_instantiations[r].left.keys() + m_no_bad_instantiations[r].right.keys())
  # if len(mets.intersection(singlets)) == 0:
    # m_no_orphans.add_reaction(m_no_bad_instantiations[r])

    

m_clean = m_clean.remove_reactions_with_metabolites(model.list_from_file_from_multiple('metabolites_to_leave_out.txt', '#'), True)
m_clean.to_tsv("second_model.tsv")