from yasmenv import model
import pandas as pd


trans_field = "transcript"
tras = pd.read_table("../peppermint_transcripts/transcript_reaction_associations.tsv", index_col = 0)

compart_map = {"plastid": "h", "cytosol":"c", "mitochondrion":"m", "peroxisome":"p"} #everything else is cytosol
get_compart = lambda s: compart_map.get(s,"c")

cyc_model_files = {"metacyc": {"metacyc_good": "../MetaCyc/instantiated_good.tsv", "metacyc_bad": "../MetaCyc/instantiated_bad.tsv"},
  "aracyc": {"aracyc_good": "../AraCyc/instantiated_good.tsv", "aracyc_bad": "../AraCyc/instantiated_bad.tsv"}}
arnold_model_file = "../Arabidopsis_Core/Arnold_metacyc_no_biomass.tsv"

def uninstantiate(m):
  out = model.Model()
  for r in m:
    r2 = m[r].copy()
    r2.name = r2.name.split("|")[0]
    out.add_reaction(r2)
  
  return out

m = model.from_tsv(arnold_model_file)

split_cyc_rxn = lambda x: x.split("|")[0]

for rxn in m:
  m.add_annotation(rxn, "source_model", "arnold")

for (i, row) in tras[tras['rxn_source'] == 'arnold'].iterrows():
  rxns = row['reactions'].split()
  for rxn in rxns:
    an = m[rxn].annotations.get(trans_field, "").split()
    an.append(row['transcript'])
    m.add_annotation(rxn, trans_field, " ".join(set(an)))
    

for (cyc, file_dict) in cyc_model_files.iteritems():
  for (model_name, model_file) in file_dict.iteritems():
    m2 = model.from_tsv(model_file)
    for (i, row) in tras[tras['rxn_source'] == cyc].iterrows(): #iterate through transcription_reaction_association 
      rxns = set(row['reactions'].split())
      compartments = set(map(get_compart, row['compartments'].split(",")))

      if len(compartments) == 0:
        print("Error, no compartments for transcript: " + row["transcript"])
      for rxn in rxns:
        for c in compartments:
          for cyc_rxn in m2:
            if rxn == split_cyc_rxn(cyc_rxn):
              new_r = model.compartmentalize_reaction(m2[cyc_rxn], c)
              
              annot = ""
              source = model_name
              new_r.name = new_r.name + "_" + source + "_" + c
              
              if new_r.name not in m:
                new_r.annotations[trans_field] = row['transcript']
                new_r.annotations["source_model"] = source
                m.add_reaction(new_r)
              else:
                trans_annot = set(m[new_r.name].annotations.get(trans_field, "").split())
                trans_annot.add(row['transcript'])
                
                source_annot = set(m[new_r.name].annotations.get("source_model", "").split())
                source_annot.add(source)
                
                #print(new_r.name)
                #print(trans_annot)
                m.add_annotation(new_r.name, trans_field, " ".join(trans_annot))
                m.add_annotation(new_r.name, "source_model", " ".join(source_annot))
m.merge_annotations("EC", "EC Number", inplace=True)
m.merge_annotations("Description", "Rxn description", sep=",", inplace=True)
m.merge_annotations("Subsystems", "Subsystem", sep=",", inplace=True)
m.remove_annotation("Reversible", inplace=True)
m.remove_annotation("ancestors", inplace=True)
m.remove_annotation("parents", inplace=True)
m.remove_annotation("Confidence Score", inplace=True)
                
m.to_tsv("first_model.tsv")