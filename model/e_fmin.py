import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib
#import yasmenv.linearprogramming as solver_lib
from yasmenv import util
import math

inf = float('inf')

solver = solver_lib.Solver()
m = model.from_tsv('sixth_model.tsv')

out_suffix = "e_fmin"

m['Ex_Raffinose'].lb = -10000
#m['Ex_Raffinose'].ub = 0

#solve the fba problem
fbasol = solver.e_fmin(m, "Biomass")
#fbasol = solver.e_fmin(m, "Ex_menthol")
print(fbasol['obj'])
print(util.format_fba(fbasol))
util.to_file(out_suffix + "_e_fmin_sol.tsv", "'" + str(fbasol['obj']) + "\n")
util.to_file(out_suffix + "_e_fmin_sol.tsv",util.format_fba(fbasol), append = True)

m2 = model.Model()
for (rxn, flux) in fbasol['fluxes']:
  if abs(flux) > 0.01:
    m2.add_reaction(m[rxn])
    m2.add_annotation(rxn, "flux", str(flux))
    m2.add_annotation(rxn, "abs_flux", str(abs(flux)))

ignore = ['ATP', 'NADP', 'NADPH', 'NAD', 'NADH', 'WATER', 'ADP', 'AMP', 'ATP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
m2.to_sif(out_suffix + '.sif', ignore, fbasol['fluxes'], ignore_compartmentalization=True)
m2.to_tsv(out_suffix + '_model.tsv')
#m.to_tsv('fba_model.tsv')



