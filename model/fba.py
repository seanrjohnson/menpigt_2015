import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib
from yasmenv.modelreaction import ModelReaction
#import yasmenv.linearprogramming as solver_lib
from yasmenv import util

inf = float('inf')

solver = solver_lib.Solver()
#m = model.from_tsv('fifth_model.tsv')
#m = model.from_multiple('model_production_files.txt')
m = model.from_tsv('sixth_model.tsv')
#m = model.from_tsv('third_model.tsv')
#m = model.from_multiple('model_with_extra.txt')
#m = model.from_tsv('fifth_model_2.tsv')
#m = model.from_multiple('model_with_supplements.txt')
#m = m.remove_metabolites(["Pi"],all_compartments=True)
#m = m.remove_metabolites(["PROTON[m]","PROTON[h]","PROTON[c]"],all_compartments=False) 
#zero out the objective function
out_suffix = "6_9_28_1"
out_string = ""

for r_name in m:
    m[r_name].objective = 0

#limit glucose uptake
#m['Ex_Glc'].lb = -10
m['Ex_Suc'].lb = 0
m['Ex_Suc'].ub = 0
#m['Im_NH4'].lb = 0
#m['Im_NH4'].ub = 0
m['Ex_Raffinose'].lb = -1000
m['Ex_Raffinose'].ub = 0
m['Ex_Raffinose'].objective = 1


m['Biomass'].objective = 0
m['Biomass'].lb = 1
m['Biomass'].ub = 1
m['Ex_menthol'].objective = 0
m['Ex_menthol'].lb = 0
m['Ex_menthol'].ub = 0

#solve the fba problem
fbasol = solver.fba(m, maximize=True, return_lp=False, minimize_flux=True)
out_string += str(fbasol['obj']) + "\n"
#print(util.format_fba(fbasol))

m2 = model.Model()
for (rxn, flux) in fbasol['fluxes']:
  if abs(flux) > 0.01:
    m2.add_reaction(m[rxn])
    m2.add_annotation(rxn, "flux", str(flux))
    m2.add_annotation(rxn, "abs_flux", str(abs(flux)))
out_string += m2.compartment_net("m", "flux").as_str() + "\n"
out_string += m2.compartment_net("h", "flux").as_str() + "\n"
out_string += m2.compartment_net("c", "flux").as_str() + "\n"
out_string += m2.compartment_net("p", "flux").as_str() + "\n"

ignore = ['ATP', 'NADP', 'NADPH', 'NAD', 'NADH', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
#ignore = ['NADP', 'NAD', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
m2.to_sif('fba_graph_' + out_suffix + '.sif', ignore, fbasol['fluxes'], ignore_compartmentalization=True)
m2.to_tsv("fba_model_" + out_suffix + ".tsv")
util.to_file("fba_sol_" + out_suffix + ".tsv", out_string)