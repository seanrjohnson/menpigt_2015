import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib
from yasmenv import util
from yasmenv.modelreaction import ModelReaction

inf = float('inf')

solver = solver_lib.Solver()
#m = model.from_multiple('model_production_files.txt')
third = model.from_tsv('third_model.tsv')
m = model.from_tsv("manual_reactions_to_generate_model_three.tsv")
#ppp = model.from_tsv("sub_pathways/pentose_phosphate.tsv")
ppp = model.from_tsv("sub_pathways/ppp_cytosol.tsv")
m.merge_in(ppp)

m.merge_in(third)

m = m.merge_metabolites({'RIBOSE-5P': ['CPD-15895','CPD-15318','CPD-16551']}, True)

#m = m.remove_metabolites(["Pi","WATER"],all_compartments=True)
#m = m.remove_metabolites(["PROTON[m]","PROTON[h]","PROTON[c]"],all_compartments=False) 

out_suffix = "energy_balance_cNADH_11_24_2015_1"


#zero out the objective function
for r_name in m:
    m[r_name].objective = 0

m['Ex_Suc'].lb = 0
m['Ex_Suc'].ub = 0
m['Ex_Raffinose'].lb = -100
m['Ex_Raffinose'].ub = 0
m['Ex_Raffinose'].objective = 1


#uncomment to test for ATP producing loops

#cytosol ATP burn
# m['3.6.3.15-RXN|0_aracyc_good_c'].ub = 300
# m['3.6.3.15-RXN|0_aracyc_good_c'].lb = 300

#mitochondrion ATP burn
# m['3.6.3.20-RXN|0_aracyc_good_m'].lb = 300
# m['3.6.3.20-RXN|0_aracyc_good_m'].ub = 300

#plastid ATP burn
# m['3.6.3.25-RXN|0_aracyc_good_h'].ub = 300
# m['3.6.3.25-RXN|0_aracyc_good_h'].lb = 300



#uncomment to test for NADH and NADPH producing loops
#these are not balanced, we ignore the H2 gas that should be being produced

# r = ModelReaction("NADPH_burn_m",  left_side={"NADPH[m]":1}, right_side={"NADP[m]":1}, lb=300, ub=300, objective=0)
# m.add_reaction(r)

# r = ModelReaction("NADPH_burn_c",  left_side={"NADPH[c]":1}, right_side={"NADP[c]":1}, lb=300, ub=300, objective=0)
# m.add_reaction(r)

# r = ModelReaction("NADPH_burn_h",  left_side={"NADPH[h]":1}, right_side={"NADP[h]":1}, lb=300, ub=300, objective=0)
# m.add_reaction(r)



# r = ModelReaction("NADH_burn_m",  left_side={"NADH[m]":1}, right_side={"NAD[m]":1}, lb=300, ub=300, objective=0)
# m.add_reaction(r)

r = ModelReaction("NADH_burn_c",  left_side={"NADH[c]":1}, right_side={"NAD[c]":1}, lb=300, ub=300, objective=0)
m.add_reaction(r)

# r = ModelReaction("NADH_burn_h",  left_side={"NADH[h]":1}, right_side={"NAD[h]":1}, lb=300, ub=300, objective=0)
# m.add_reaction(r)



#add conservation pseudo-metabolites... Nope that was a bad plan. that didn't work at all...

# m = m.add_linked_metabolite(['NADP[h]'],['NADPH[h]'],'NADP_conserve[h]','NADPH[h]')
# m = m.add_linked_metabolite(['NADP[c]'],['NADPH[c]'],'NADP_conserve[c]','NADPH[c]')
# m = m.add_linked_metabolite(['NADP[m]'],['NADPH[m]'],'NADP_conserve[m]','NADPH[m]')
# m = m.add_linked_metabolite(['NADP[p]'],['NADPH[p]'],'NADP_conserve[p]','NADPH[p]')

# m = m.add_linked_metabolite(['NAD[h]'],['NADH[h]'],'NAD_conserve[h]','NADH[h]')
# m = m.add_linked_metabolite(['NAD[c]'],['NADH[c]'],'NAD_conserve[c]','NADH[c]')
# m = m.add_linked_metabolite(['NAD[m]'],['NADH[m]'],'NAD_conserve[m]','NADH[m]')
# m = m.add_linked_metabolite(['NAD[p]'],['NADH[p]'],'NAD_conserve[p]','NADH[p]')

# m.to_tsv('conservation_metabolite_test.tsv')

solution = solver.fba(m, maximize=False)

fbasol = solver.fba(m, maximize=True, return_lp=False, minimize_flux=True)
#print(fbasol['obj'])
util.to_file(out_suffix + "_fba_sol.tsv", util.format_fba(fbasol))
m2 = model.Model()

for (rxn, flux) in fbasol['fluxes']:
  if abs(flux) > 0.01:
    m2.add_reaction(m[rxn])
    m2.add_annotation(rxn, "flux", str(flux))
    m2.add_annotation(rxn, "abs_flux", str(abs(flux)))

ignore = ['WATER', 'PROTON']
m2.to_sif(out_suffix + '.sif', ignore, fbasol['fluxes'], ignore_compartmentalization=True)
m2.to_tsv(out_suffix + '_model.tsv')