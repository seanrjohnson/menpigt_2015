import yasmenv.model as model
import pandas as pd

model_four = model.from_tsv('fourth_model.tsv')
manual = model.from_tsv("manual_reactions_to_generate_model_six.tsv")
manual.merge_in(model_four.copy())
(m, clean_map) = manual.clean(merge_args={'metabolites_merge':max, 'annotations_sep':{"Description":",", "Subsystems":",", "Gene-reaction association":','}})

for r in m:
  old_rxns = set()
  for (r_old, r_new) in clean_map.iteritems():
    if r_new is not None and r_new['name'] == r:
      old_rxns.add(r_old)
  m.add_annotation(r, "merged_reactions", " ".join(old_rxns))

singlet_metabolites = m.find_singlets()
while len(singlet_metabolites) > 0:
  m = m.remove_reactions_with_metabolites(singlet_metabolites)
  singlet_metabolites = m.find_singlets()

m = m.make_infinite()  

an = pd.read_csv("../peppermint_transcripts/annotations.csv", index_col=0)

#get rid of some cruft
# m = m.merge_annotations("Subsystems", "Subsystem")
# m = m.remove_annotation("ancestors", "parents")
# m = m.remove_annotation("Confidence Score", "parents")
# m = m.remove_annotation("Reversible", "parents")
# m = m.remove_annotation("Spontatneous", "parents")

for (field, column) in {"Expression_mature": "TPM mature", "Expression": "TPM"}.items():
  for r in m:
    transcripts = set(m[r].get_annotations("transcript"))
    print(r)
    exp=0.0
    for transcript in transcripts:
      if transcript != "":
        exp += an.ix[transcript, column]
    m.add_annotation(r, field, exp)
    
m.to_tsv("sixth_model.tsv")