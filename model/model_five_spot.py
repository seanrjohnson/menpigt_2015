import yasmenv.model as model
from pprint import pprint as pp
import yasmenv.modelreaction as modelreaction
import yasmenv.gurobi_solver as solver_lib
from yasmenv.modelreaction import ModelReaction
#import yasmenv.linearprogramming as solver_lib
from yasmenv import util
import pandas as pd

inf = float('inf')

m = model.from_tsv('fifth_model.tsv')
allow_antifluxes=False
out_suffix = "model_five_spot_full_output"
out_string = ""

solver = solver_lib.Solver()


#zero out the objective function
for r_name in m:
    m[r_name].objective = 0

#Set boundary fluxes
m['Ex_Suc'].lb = -1000
m['Ex_Suc'].ub = 0
m['Ex_Glc'].lb = -1000
m['Ex_Glc'].ub = 0
m['Ex_Frc'].lb = -1000
m['Ex_Frc'].ub = 0
m['Im_NH4'].lb = -1000
m['Im_NH4'].ub = 1000
m['Ex_Raffinose'].lb = -1000
m['Ex_Raffinose'].ub = 0


m['Biomass'].objective = 0
m['Biomass'].lb = 0
m['Biomass'].ub = 0
m['Ex_menthol'].objective = 0
m['Ex_menthol'].lb = 0
m['Ex_menthol'].ub = 1000


for (condition, field) in {"mature": "Expression_mature", "young": "Expression"}.items():
    fbasol = solver.spot(m, field, allow_antifluxes, return_lp=False)

    out_string += str(fbasol['obj']) + "\n"
    #print(util.format_fba(fbasol))

    m2 = model.Model()
    for (rxn, flux) in fbasol['fluxes']:
        #if abs(flux) > 0.0001:
        m2.add_reaction(m[rxn])
        m2.add_annotation(rxn, "flux", str(flux))
        m2.add_annotation(rxn, "abs_flux", str(abs(flux)))
    out_string += m2.compartment_net("m", "flux").as_str() + "\n"
    out_string += m2.compartment_net("h", "flux").as_str() + "\n"
    out_string += m2.compartment_net("c", "flux").as_str() + "\n"
    out_string += m2.compartment_net("p", "flux").as_str() + "\n"

    ignore = ['ATP', 'NADP', 'NADPH', 'NAD', 'NADH', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
    #ignore = ['NADP', 'NAD', 'WATER', 'ADP', 'AMP', 'PROTON', 'Pi', 'HYDROGEN-PEROXIDE', 'CARBON-DIOXIDE', 'CO-A', 'PPI', 'OXYGEN-MOLECULE', "Biomass"]
    m2.to_sif('fba_graph_' + condition + "_" + out_suffix + '.sif', ignore, fbasol['fluxes'], ignore_compartmentalization=True)
    m2.to_tsv("fba_model_" + condition + "_" + out_suffix + ".tsv")
    util.to_file("fba_sol_" + condition + "_" + out_suffix + ".tsv", out_string)