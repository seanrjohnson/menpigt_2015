import pandas as pd
import numpy as np
from yasmenv import model
gra_parse = model.GraParser()

source_models = ("instantiated_good.tsv","instantiated_bad.tsv")
model_protein_col = "Genes"
model_EC_col = "EC"
genes_processor = lambda x: str(x).strip().split()#gra_parse.get_genes_from_bool
reaction_processor = lambda x: x.split("|")[0]
EC_processor = lambda x: str(x).strip().split()

outfile_name = "aracyc_prot_to_rxn.tsv"

def uninstantiate(m):
  out = model.Model()
  for r in m:
    r2 = m[r].copy()
    r2.name = r2.name.split("|")[0]
    out.add_reaction(r2)
  
  return out

ECs = dict() #keys are gene names, values are sets of ECs
rxns = dict() #keys are gene names, values are sets of ECs

for file_name in source_models:
  m = model.from_tsv(file_name)
  m = uninstantiate(m)
  for r in m:
    genes = set(genes_processor(m[r].annotations[model_protein_col]))
    
    for gene in genes:
      if gene not in ECs:
        ECs[gene] = set()
      if gene not in rxns:
        rxns[gene] = set()
    
      rxns[gene].add(reaction_processor(r))
      ECs[gene] |= set(EC_processor(m[r].annotations[model_EC_col]))
  
with open(outfile_name, "w") as of:
  of.write("gene"+"\t"+"ECs"+"\t"+"Reactions\n")
  for (gene, ecs) in ECs.iteritems():
    ec_out = {x for x in ECs[gene] if x != "--"}
    of.write(gene+"\t"+" ".join(ec_out)+"\t"+" ".join(rxns[gene]) + "\n")
  
